package com.noosyn.learning.meeting;

import lombok.Value;

@Value(staticConstructor = "of")
public class JoinCommand {
    Integer personId;
    Integer meetingId;
}
