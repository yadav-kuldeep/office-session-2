package com.noosyn.learning.meeting;

import lombok.Value;

@Value(staticConstructor = "of")
public class MeetingPolicy {
    Meeting meeting;

    public boolean isSatisfiedBy(Person person) {
        return false;
    }
}
