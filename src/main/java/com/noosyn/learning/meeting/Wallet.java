package com.noosyn.learning.meeting;

import lombok.Value;

import java.math.BigDecimal;

@Value(staticConstructor = "of")
class Wallet {
    BigDecimal balance;
}
