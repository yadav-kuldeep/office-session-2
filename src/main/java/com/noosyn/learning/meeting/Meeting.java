package com.noosyn.learning.meeting;

import lombok.Value;

import java.util.List;

@Value(staticConstructor = "of")
public class Meeting {
    MeetingId meetingId;
    List<PersonId> personsAllowed;

    public void accept(Person person) {

    }
}
