package com.noosyn.learning.meeting;

import lombok.Value;

@Value(staticConstructor = "of")
public class PersonId {
    Integer value;
}
