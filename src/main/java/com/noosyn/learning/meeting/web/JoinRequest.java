package com.noosyn.learning.meeting.web;

import lombok.Data;

@Data
class JoinRequest {
    Integer personId;
    Integer meetingId;
}
