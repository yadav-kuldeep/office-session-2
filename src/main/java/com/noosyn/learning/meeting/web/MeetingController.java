package com.noosyn.learning.meeting.web;

import com.noosyn.learning.meeting.JoinCommand;
import com.noosyn.learning.meeting.Meeting;
import com.noosyn.learning.meeting.service.MeetingApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
class MeetingController {
    private final WebMapper mapper;
    private final MeetingApplicationService service;

    @PostMapping("/join")
    public ResponseEntity<String> joinMeeting(JoinRequest request) {
        JoinCommand cmd = mapper.map(request);
        Meeting meeting = service.joinMeeting(cmd);
        return ResponseEntity.status(HttpStatus.CREATED).body("Success joined : " + meeting.getMeetingId().getValue());
    }
}
