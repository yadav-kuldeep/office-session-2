package com.noosyn.learning.meeting;

import lombok.Value;

@Value(staticConstructor = "of")
public class Person {
    PersonId personId;
    String name;
    Wallet wallet;
    boolean isActive;
}
