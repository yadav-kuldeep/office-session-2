package com.noosyn.learning.meeting.service;

import com.noosyn.learning.meeting.JoinCommand;
import com.noosyn.learning.meeting.Meeting;
import com.noosyn.learning.meeting.MeetingId;
import com.noosyn.learning.meeting.MeetingPolicy;
import com.noosyn.learning.meeting.MeetingRepository;
import com.noosyn.learning.meeting.Person;
import com.noosyn.learning.meeting.PersonId;
import com.noosyn.learning.meeting.PersonRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MeetingApplicationService {
    private final MeetingRepository meetingRepository;
    private final PersonRepository personRepository;

    public Meeting joinMeeting(JoinCommand cmd) {
        MeetingId meetingId = MeetingId.of(cmd.getMeetingId());
        PersonId personId = PersonId.of(cmd.getPersonId());

        Meeting meeting = meetingRepository.findById(meetingId);
        Person person = personRepository.findById(personId);

        MeetingPolicy policy = MeetingPolicy.of(meeting);

        if(policy.isSatisfiedBy(person)) {
            meeting.accept(person);
            return meetingRepository.save(meeting);
        }

        throw new IllegalStateException("Could not join person");
    }
}
