package com.noosyn.learning.meeting;

public interface PersonRepository {
    Person findById(PersonId id);
    Person save(Person person);
}
