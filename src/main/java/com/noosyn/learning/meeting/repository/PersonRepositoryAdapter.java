package com.noosyn.learning.meeting.repository;

import com.noosyn.learning.meeting.Person;
import com.noosyn.learning.meeting.PersonId;
import com.noosyn.learning.meeting.PersonRepository;
import org.springframework.stereotype.Component;

@Component
class PersonRepositoryAdapter implements PersonRepository {
    @Override
    public Person findById(PersonId id) {
        return null;
    }

    @Override
    public Person save(Person person) {
        return null;
    }
}
