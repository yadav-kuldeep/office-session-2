package com.noosyn.learning.meeting.repository;

import com.noosyn.learning.meeting.repository.entities.MeetingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaMeetingRepository extends JpaRepository<MeetingEntity, Integer> {
}
