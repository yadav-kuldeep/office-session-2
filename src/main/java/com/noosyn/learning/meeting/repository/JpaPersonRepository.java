package com.noosyn.learning.meeting.repository;

import com.noosyn.learning.meeting.repository.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaPersonRepository extends JpaRepository<PersonEntity, Integer> {
}
