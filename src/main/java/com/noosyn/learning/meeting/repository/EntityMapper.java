package com.noosyn.learning.meeting.repository;

import com.noosyn.learning.meeting.Meeting;
import com.noosyn.learning.meeting.MeetingId;
import com.noosyn.learning.meeting.PersonId;
import com.noosyn.learning.meeting.repository.entities.MeetingEntity;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toList;

@Component
class EntityMapper {
    Meeting map(MeetingEntity entity) {
        return Meeting.of(
                MeetingId.of(entity.getId()),
                entity.getPersons().stream().map(p -> PersonId.of(p.getId())).collect(toList())
        );
    }

    MeetingEntity map(Meeting meeting) {
        return null;
    }
}
