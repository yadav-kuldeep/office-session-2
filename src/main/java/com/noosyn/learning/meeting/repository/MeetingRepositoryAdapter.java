package com.noosyn.learning.meeting.repository;

import com.noosyn.learning.meeting.Meeting;
import com.noosyn.learning.meeting.MeetingId;
import com.noosyn.learning.meeting.MeetingRepository;
import com.noosyn.learning.meeting.repository.entities.MeetingEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
class MeetingRepositoryAdapter implements MeetingRepository {
    private final JpaMeetingRepository meetingRepository;
    private final EntityMapper mapper;

    @Override
    public Meeting findById(MeetingId id) {
        final MeetingEntity meeting = meetingRepository.findById(id.getValue())
                .orElseThrow(() -> new IllegalArgumentException("Meeting not found"));
        return mapper.map(meeting);
    }

    @Override
    @Transactional
    public Meeting save(Meeting meeting) {
        MeetingEntity entity = mapper.map(meeting);
        MeetingEntity savedEntity = meetingRepository.save(entity);
        return mapper.map(savedEntity);
    }
}
