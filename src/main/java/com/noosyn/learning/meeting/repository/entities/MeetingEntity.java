package com.noosyn.learning.meeting.repository.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class MeetingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(
            cascade = {CascadeType.MERGE, CascadeType.REMOVE},
            mappedBy = "meeting"
    )
    private List<PersonEntity> persons = new ArrayList<>();

    public void addPerson(PersonEntity person) {
        persons.add(person);
        person.setMeeting(this);
    }

}
