package com.noosyn.learning.meeting.repository.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.math.BigDecimal;

import static javax.persistence.FetchType.LAZY;
import static lombok.AccessLevel.PRIVATE;

@Entity
@Getter
@NoArgsConstructor(access = PRIVATE)
public class PersonEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean isActive;
    private BigDecimal balance;

    public PersonEntity(Integer id, String name, Boolean isActive, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.balance = balance;
    }

    static PersonEntity of(Integer id, String name, Boolean isActive, BigDecimal balance) {
        return new PersonEntity(id, name, isActive, balance);
    }

    @OneToOne(fetch = LAZY)
    @Setter
    private MeetingEntity meeting;
}
