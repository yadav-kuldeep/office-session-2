package com.noosyn.learning.meeting;

public interface MeetingRepository {
    /**
     * Gives back meeting object
     * @param id MeetingId
     * @return Meeting
     * @throws NotFountException
     */
    Meeting findById(MeetingId id);
    Meeting save(Meeting meeting);
}
