# Session: Functional programming using vavr and package structure #

This readme file will get you started on this project.

### What is this repository for? ###

* We will learn some functional programming using library such as vavr
* We will try to enforce package structure which is near to feature/function

### How do I get set up? ###

* Clone the repository to your local machine
* Open with eclipse or Intelij Idea
* Find `Application` class and run like a normal java class with main function
* To run tests just right click on the test folder and select `run`

### Alternative way to run
> mvn springboot:run
